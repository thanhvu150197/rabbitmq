﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;

namespace RabbitDemo.Controllers
{
    
        [Route("/RabbitMQ")]
        [ApiController]
        public class RabbitMQController:ControllerBase
        {
         
           [HttpGet("Send")]
           public IActionResult SendMess()
            {
                CommonFunc.Send("Queue1", "Messenger");
                return Ok("Send Ok");
            }
           [HttpGet("Receive")]
           public IActionResult Receive()
           {           
                return Ok(CommonFunc.Receive("Queue1"));
           }
        
        }
      
    
}